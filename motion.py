#!/usr/bin/env python3

import RPi.GPIO as GPIO
import time
import logging
import signal, sys
from argparse import ArgumentParser

logFormatter = logging.Formatter("%(asctime)s [%(name)-8.8s]/[%(funcName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger(__name__)

fileHandler = logging.FileHandler("{0}/{1}.log".format("./", "piBell"), 'a')
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)

GPIO.setmode(GPIO.BCM)
pir = 21
GPIO.setup(pir, GPIO.IN)

def main():
    motion = Motion(debugMode) 
    while True:
        if motion.detectMotion():
            time.sleep(3)
        time.sleep(0.01)
    
def _get_args():
    parser = ArgumentParser(description='piBell overrides')
    parser.add_argument('-v', '--verbose', help='Output level. "INFO" is the default. Supports: "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"', default="INFO")
    return parser.parse_args()

def _signal_handler(signal, frame):
    print ()
    print("Exiting application.")
    sys.exit(0)

class Motion(object):
    def __init__(self, debugMode):
        rootLogger.setLevel(debugMode)
        signal.signal(signal.SIGINT, _signal_handler)
        
    def detectMotion(self):
        if GPIO.input(pir):
            rootLogger.info("Motion has been detected.")
            return True
        else:
            rootLogger.debug("NO Motion Detected.")
            return False
    
if __name__ == "__main__":
    args = _get_args() 
    debugMode = args.verbose
    rootLogger.setLevel(debugMode)
    signal.signal(signal.SIGINT, _signal_handler)
    main()
