#!/usr/bin/python3

import logging
import boto3
import json
from argparse import ArgumentParser

logFormatter = logging.Formatter("%(asctime)s [%(name)-8.8s]/[%(funcName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger(__name__)

fileHandler = logging.FileHandler("{0}/{1}.log".format("./", "piBell"), 'a')
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)

def main():
    args = _get_args()
    sms = SMS(args.access, args.secret, args.region, args.message, args.number, args.verbose)
    print(sms.send_sms())

def _get_args():
    parser = ArgumentParser(description='AWS sns')
    
    parser.add_argument('-s', '--secret', help='AWS secret access key')
    parser.add_argument('-a', '--access', help='AWS access key id')
    parser.add_argument('-r', '--region', default='us-west-2', help='AWS region. Default: us-west-2')
    parser.add_argument('-v', '--verbose', help='Output level. "INFO" is the default. Supports: "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"', default="INFO")
    parser.add_argument('-n', '--number', help='Phone Number to be used for SMS. If no Number is set, No text will be sent. (Format = +12223334444)', default="none")
    parser.add_argument('-m', '--message', help='Message body for the Text, "Test Message" is the default', default="Test Message")
    return parser.parse_args()


class SMS(object):
    def __init__(self, awsAccessKey, awsSecAccessKey, awsRegion="us-west-2", textMessage="Test Message", phoneNumber="none", debugMode="INFO"):
        rootLogger.setLevel(debugMode)
        self.message = textMessage
        self.number = phoneNumber
        self.__client = boto3.client('sns')

    def send_sms(self):
        if self.number == 'none':
            rootLogger.debug('No Number provided, Not Attempting to send SMS.')
            return "No Number provided, Not Attempting to send SMS."
        else: 
            smsresponse = self.__client.publish(PhoneNumber=self.number, Message=self.message)
            rootLogger.info("Sending SMS.")
            rootLogger.debug('Number: ' + self.number)
            rootLogger.debug('Message: ' + self.message) 
            rootLogger.debug('AWS Response: ' + str(smsresponse))
            return smsresponse
    
if __name__ == "__main__":
    main()

