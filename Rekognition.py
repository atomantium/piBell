#!/usr/bin/env python3

import boto3
import logging
import json
from argparse import ArgumentParser
from contextlib import closing
from botocore.exceptions import BotoCoreError, ClientError

logFormatter = logging.Formatter("%(asctime)s [%(name)-8.8s]/[%(funcName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger(__name__)

fileHandler = logging.FileHandler("{0}/{1}.log".format("./", "piBell"), 'a')
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)


def main():
    args = _get_args()
    rekognition = Rekognition(args.access, args.secret, args.region, args.verbose)
    print(json.dumps(rekognition.get_labels()))

def _get_args():
    parser = ArgumentParser(description='AWS rekognition')
    
    parser.add_argument('-s', '--secret', help='AWS secret access key')
    parser.add_argument('-a', '--access', help='AWS access key id')
    parser.add_argument('-r', '--region', default='us-west-2', help='AWS region. Default: us-west-2')
    parser.add_argument('-v', '--verbose', help='Output level. "INFO" is the default. Supports: "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"', default="INFO")
    return parser.parse_args()

class Rekognition(object):
    def  __init__(self, awsAccessKey, awsSecAccessKey, awsRegion="us-west-2", debugMode="INFO"):
        self.awsAccessKey = awsAccessKey
        self.awsSecAccessKey = awsSecAccessKey
        self.awsRegion = awsRegion
        self.filePath = "./image.jpg"
        self.minFaceConf = 75
        self.minCelebConf = 25
        self.__rek = boto3.client('rekognition', region_name=awsRegion, aws_access_key_id=awsAccessKey, aws_secret_access_key=awsSecAccessKey)
        rootLogger.setLevel(debugMode)

    def _get_image(self):
        try:
            with open(self.filePath, 'rb') as imgfile:
                return imgfile.read()
        except Exception as e:
            rootLogger.error("Unable to open image file: %s", e)
    
    def get_labels(self):
        rootLogger.debug("filePath: " + self.filePath)
        rootLogger.debug("minCelebConf: " + str(self.minCelebConf))
        try:
            rawImage = self._get_image()
            rekresponse = self.__rek.detect_labels(Image={'Bytes': rawImage},MinConfidence=self.minFaceConf)
            return rekresponse['Labels']
        except (BotoCoreError, ClientError) as error:
            rootLogger.error("Unable to get rekognition labels: %s", error)
    
    def get_faces(self):
        try:
            rawImage = self._get_image()
            rekresponse = self.__rek.detect_faces(Image={'Bytes': rawImage})
            return rekresponse['FaceDetails']
        except (BotoCoreError, ClientError) as error:
            rootLogger.error("Unable to get rekognition faces: %s", error)
    
    def get_celebrities(self):
        try:
            rawImage = self._get_image()
            rekresponse = self.__rek.recognize_celebrities(Image={'Bytes': rawImage})
            return rekresponse['CelebrityFaces']
        except (BotoCoreError, ClientError) as error:
            rootLogger.error("Unable to get rekognition celebrities: %s", error)

if __name__ == "__main__":
    main()

