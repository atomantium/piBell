#!/usr/bin/env python3

import pygame
import io
import boto3
import logging
from argparse import ArgumentParser
from contextlib import closing
from botocore.exceptions import BotoCoreError, ClientError

logFormatter = logging.Formatter("%(asctime)s [%(name)-8.8s]/[%(funcName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger(__name__)

fileHandler = logging.FileHandler("{0}/{1}.log".format("./", "piBell"), 'a')
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)


def _main():
    args = _get_args()
    synthesizer = Synthesizer(args.access, args.secret, args.region, args.voice, args.verbose)
    synthesizer.Say("So that's how it's gonna be? You done messed up ay ay ron!")    

def _get_args():
    parser = ArgumentParser(description='AWS rekognition')
    
    parser.add_argument('-c', '--voice', default="Brian", help='AWS polly voice character')
    parser.add_argument('-s', '--secret', help='AWS secret access key')
    parser.add_argument('-a', '--access', help='AWS access key id')
    parser.add_argument('-r', '--region', default='us-west-2', help='AWS region. Default: us-west-2')
    parser.add_argument('-v', '--verbose', help='Output level. "INFO" is the default. Supports: "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"', default="INFO")
    return parser.parse_args()

class Synthesizer(object):
    def __init__(self, awsAccessKey, awsSecAccessKey, awsRegion='us-west-2', voiceId="Brian", debugMode="INFO"):
        pygame.mixer.init(48000, -16, 1, 1024)
        pygame.mixer.stop()
        self.awsAccessKey = awsAccessKey
        self.awsSecAccessKey = awsSecAccessKey
        self.awsRegion = awsRegion
        self._volume = 1
        self.voice = voiceId
        self.__polly = boto3.client("polly", region_name=awsRegion, aws_access_key_id=awsAccessKey, aws_secret_access_key=awsSecAccessKey)
        rootLogger.setLevel(debugMode)
 
    def _GetVolume(self):
        return self._volume
 
    def Say(self, text):
        self._Synthesize(text)
        rootLogger.debug("Text set: " + text)
 
    def _Synthesize(self, text):
        rootLogger.info("Sending text to polly.")
        try:
            response = self.__polly.synthesize_speech(Text=text, OutputFormat="ogg_vorbis", VoiceId=self.voice)
            rootLogger.info("Received audio stream responce from polly")
            
            if "AudioStream" in response:
                with closing(response["AudioStream"]) as stream:
                    data = stream.read()
                    filelike = io.BytesIO(data)
                    sound = pygame.mixer.Sound(file=filelike)
                    sound.set_volume(self._GetVolume())
                    sound.play() 
                    while pygame.mixer.get_busy() == True:
                        continue
                    pygame.mixer.stop()
                    rootLogger.info("Audio stream playback complete.")
            else:
                rootLogger.error("Unable to stream audio. No audio data in response.")
        except (BotoCoreError, ClientError) as error:
            rootLogger.error("Error obtaining response from polly: " + str(error))


if __name__ == "__main__":
    _main()
