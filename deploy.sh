#1/bin/bash
if [ -e /opt/piBell ]
then
  sudo cp -v ./*.py /opt/piBell/
  sudo chown pi.pi -vR /opt/piBell
  sudo chmod g+sw -v /opt/piBell 
else
  sudo mkdir -v /opt/piBell 
  sudo cp -v ./*.py /opt/piBell/
  sudo chown pi.pi -vR /opt/piBell
  sudo chmod g+sw -v /opt/piBell 
fi

