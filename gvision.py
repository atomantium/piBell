#!/usr/bin/env python3

import logging
from argparse import ArgumentParser
from google.cloud import vision
from google.cloud.vision import types

logFormatter = logging.Formatter("%(asctime)s [%(name)-8.8s]/[%(funcName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger(__name__)

fileHandler = logging.FileHandler("{0}/{1}.log".format("./", "piBell"), 'a')
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)


def main():
    args = _get_args()
    gvision = Vision(args.image, args.verbose)
    print(gvision.google_detect_labels())
    print(gvision.google_detect_face())

def _get_args():
    parser = ArgumentParser(description='Google Vision')
    
    parser.add_argument('-i', '--image', default="./image.jpg", help="image path")
    parser.add_argument('-v', '--verbose', help='Output level. "INFO" is the default. Supports: "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"', default="INFO")
    return parser.parse_args()

class Vision(object):
    def __init__(self, imgPath='./image.jpg', debugMode="INFO"):
        self.filePath = imgPath
        self.__client = vision.ImageAnnotatorClient()
        rootLogger.setLevel(debugMode)

    def _get_image(self):
        try:
            rootLogger.debug("Attempting to open Image File: %s", self.filePath)
            with open(self.filePath, 'rb') as imgfile:
                rootLogger.debug("")
                return imgfile.read()
        except Exception as e:
            rootLogger.error("Error loading image: %s", e)
    
    def google_detect_labels(self):
        try:
            rootLogger.debug("Starting Google Detect Labels.")
            rawImage = self._get_image()
            image = types.Image(content=rawImage)
            google_response = self.__client.label_detection(image=image)
            labels = google_response.label_annotations
            rootLogger.debug(labels)
            return labels
        except Exception as e:
            rootLogger.error("Error to detect labels: %s", e)
    
    def google_detect_face(self):
        try:
            rootLogger.debug("Starting Google Detect Faces.")
            rawImage = self._get_image()
            image = types.Image(content=rawImage)
            google_response = self.__client.face_detection(image=image)
            rootLogger.debug(google_response)
            return google_response
        except Exception as e:
            rootLogger.error("Error detecting faces: %s", e)

if __name__ == "__main__":
    main()

