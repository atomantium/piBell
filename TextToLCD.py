#!/usr/bin/env python3

import sys
import time
import logging
from RPLCD.i2c import CharLCD

logFormatter = logging.Formatter("%(asctime)s [%(name)-8.8s]/[%(funcName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger(__name__)

fileHandler = logging.FileHandler("{0}/{1}.log".format("./", "piBell"), 'a')
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)


lcd = CharLCD('PCF8574', 0x27)

# Row limit of LCD display:
rowLimit = 2
# Char limit of LCD display:
charLimit = 16
# Speed of char scroll
scrollInt = 0.2
# Paus duration after text is displayed
rowInt = 0

testBuffer = ["So that's how it's going to be?", "You done messed up AAron!"]

def main():
    ProcessFrameBuffer(testBuffer, 'DEBUG')
    
def ProcessFrameBuffer(frameBuffer, debugMode='INFO'):
    rootLogger.setLevel(debugMode)
    rootLogger.debug("Begin processing framebuffer: " + str(frameBuffer))

    if len(frameBuffer) <= rowLimit:
        lcd.clear()
        rootLogger.debug("Framebuffer validation - rows less than rowLimit: " + str(rowLimit) + ".")
    
        if frameBuffer is not "":
            rootLogger.debug("Framebuffer validation - framebuffer not empty.")

            for rowNum in range(len(frameBuffer)):
                if len(frameBuffer[rowNum]) > charLimit:
                    rootLogger.debug("text: " + frameBuffer[rowNum] + ", row: " + str(rowNum) + ", chars: " + str(len(frameBuffer[rowNum])) + "/" + str(charLimit))
                    __MarqueeText(frameBuffer[rowNum], rowNum)
                    time.sleep(rowInt)
                else:
                    rootLogger.debug("text: " + frameBuffer[rowNum] + ", row: " + str(rowNum) + ", chars: " + str(len(frameBuffer[rowNum])) + "/" + str(charLimit))
                    __WriteToLCD(frameBuffer[rowNum], rowNum, 0)
    
            
            for rowNum in range(len(frameBuffer)):
                __WriteToLCD(frameBuffer[rowNum], rowNum, 0)
            time.sleep(rowInt)
        else:

            ProcessFrameBuffer(["Error:", "No text in framebuffer!"])
            rootLogger.error("No text in framebuffer.")
    else:
        ProcessFrameBuffer(["Error:", "Items in framebuffer exceed phisical display limit!"])
        rootLogger.error("Items in framebuffer exceed physical display limit.")

    rootLogger.debug("Framebuffer processing complete.")

def __MarqueeText(text, rowNum):
    newText = ""
    
    try:
        for i in range(8):
            __WriteToLCD(text[:8 + i], rowNum, 8 - i)
            time.sleep(scrollInt)
    
        for i in range(len(text) - 16 + 1):
            newText = text[i:i+16]
            __WriteToLCD(newText, rowNum, 0)
            time.sleep(scrollInt)
    except:
        rootLogger.error("Error when attempting to marquee desired text.")

def __WriteToLCD(text, row, column):
    try:
        lcd.home()
        lcd.cursor_pos = (row, column)
        lcd.write_string(text[:16])
    except:
        rootLogger.critical("Cannot write to physical display. Check connections.")

def Clear():
    lcd.home()
    lcd.clear()
    lcd.close(clear=True)
    rootLogger.info("LCD display connection closed and cleared.")

if __name__ == "__main__":
    main()
    Clear()
