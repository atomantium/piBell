#!/usr/bin/env python3

import picamera
import logging
import os


logFormatter = logging.Formatter("%(asctime)s [%(name)-8.8s]/[%(funcName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger(__name__)

fileHandler = logging.FileHandler("{0}/{1}.log".format("./", "piBell"), 'a')
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)

def main():
    GetPhoto("./image.jpg", 270, "DEBUG")

def GetPhoto(imgPath="./image.jpg", imgRotation=0, debugMode="INFO", showImage="False"):
    rootLogger.setLevel(debugMode)

    try:
        myImage = open(imgPath, 'wb')
        rootLogger.info("Starting image capture.")
        
        camera = picamera.PiCamera()
        camera.rotation = imgRotation
        camera.capture(myImage)
        myImage.close()
        camera.close()

        rootLogger.info("Image capture complete.")
        if showImage == "True":
            os.system("shellpic " + imgPath)

        return True

    except Exception as e:
        rootLogger.critical('Unable to obtain image from pi camera! %s', e)
        return False

if __name__ == "__main__":
    main()
