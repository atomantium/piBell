#!/usr/bin/env python3

import logging
from argparse import ArgumentParser
from google.cloud import storage
from google.cloud.storage import Blob

logFormatter = logging.Formatter("%(asctime)s [%(name)-8.8s]/[%(funcName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger(__name__)

fileHandler = logging.FileHandler("{0}/{1}.log".format("./", "piBell"), 'a')
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)


def main():
    args = _get_args()
    fileName = args.name
    filePath = args.file
    fileType = args.type
    bucket = args.bucket
    gproject = args.project
    debugMode = args.verbose

    gs = Storage(fileName, filePath, fileType, bucket, gproject, debugMode)
    gs.upload_from_filename()

def _get_args():
    parser = ArgumentParser(description='Google Storage')
    
    parser.add_argument('-b', '--bucket', help="Required | GCP Bucket Name")
    parser.add_argument('-f', '--file', help="Required | Full file path.")
    parser.add_argument('-n', '--name', default="YYYYMMDD-HHmmSS.jpg", help="Name to save image as in GCP. Default: YYYYMMDD-HHmmSS.jpg") # Should add logic to parse file path for default name
    parser.add_argument('-p', '--project', help="Required | GCP Project Name")
    parser.add_argument('-t', '--type', default="text/plain", help='GCP File Type. Valid options: "image/jpeg", "text/plain", "text/html", "application/octet-stream", "application/pdf"')
    parser.add_argument('-v', '--verbose', help='Output level. "INFO" is the default. Supports: "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"', default="INFO")
    return parser.parse_args()


class Storage(object):
    def __init__(self, fileName, filePath, fileType, bucket, gproject, debugMode):
        self.filePath = filePath
        self.fileName = fileName
        self.fileType = fileType
        self.bucket = bucket 
        self.project = gproject
        self.__gs = storage.Client(project=self.project)
        self.__gb = self.__gs.get_bucket(self.bucket)
        rootLogger.setLevel(debugMode)
    
    def upload_from_filename(self):
        rootLogger.debug("File: " + self.filePath)
        rootLogger.debug("Name: " + self.fileName)
        rootLogger.debug("Type: " + self.fileType)
        rootLogger.debug("Project: " + self.project)
        rootLogger.debug("Bucket: " + self.bucket)

        blob = Blob(self.fileName, self.__gb)
        try:
            blob.upload_from_filename(self.filePath, content_type=self.fileType)
        
        except Execption as e:
            rootLogger.error("Unable to upload file to Google Storage.", e)
        
if __name__ == "__main__":
    main()

