#!/usr/bin/env python3

import boto3
import logging
from argparse import ArgumentParser
from contextlib import closing
from botocore.exceptions import BotoCoreError, ClientError

logFormatter = logging.Formatter("%(asctime)s [%(name)-8.8s]/[%(funcName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger(__name__)

fileHandler = logging.FileHandler("{0}/{1}.log".format("./", "piBell"), 'a')
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)


def main():
    args = _get_args()
    rekcollect = RekCollection(args.access, args.secret, args.region, args.verbose)
    rekcollect.collectionName = args.collection
    rekcollect.filePath = args.image
    rekcollect.label = args.label
    rekcollect.faceID = args.faceID

    if args.function == "collection-create":
        response = rekcollect.create_collection()
        try:
            print("Successfully created collection: " + response['CollectionArn'])
        except:
            print(response)

    if args.function == "collection-delete":
        response = rekcollect.delete_collection()
        try:
            if response['StatusCode'] == 200:
                print("Successfully removed collection: " + args.collection)
            else:
                print("Unable to remove collection: " + args.collection)
        except:
            print(response)

    if args.function == "collection-list":
        response = rekcollect.list_collection()
        try:
            print("List of collections successful: " + str(response['CollectionIds']))
        except:
            print(response)

    if args.function == "face-index":
        response = rekcollect.index_face()
        try:
            print("==================[ Indexed Successfully ]==")
            for faceRecord in response['FaceRecords']:
                print("--[ " + faceRecord['Face']['ExternalImageId'] + " ]--")
                print("ExternalImageId: " + faceRecord['Face']['ExternalImageId'])
                print("ImageId: " + faceRecord['Face']['ImageId'])
                print("FaceId: " + faceRecord['Face']['FaceId'])
                print("--------------------\n")
            print("=============================")
        except:
            print(response)

    if args.function == "face-find":
        response = rekcollect.find_face()
        try:
            print("==================[ Faces Found ]==")
            for faceMatch in response['FaceMatches']:
                print("--[ " + faceMatch['Face']['ExternalImageId'] + " ]--")
                print("ExternalImageId: " + faceMatch['Face']['ExternalImageId'])
                print("ImageId: " + faceMatch['Face']['ImageId'])
                print("FaceId: " + faceMatch['Face']['FaceId'])
                print("Similarity: " + str(faceMatch['Similarity']))
                print("--------------------\n")
            print("=============================")
        except:
            print(response)

    if args.function == "face-list":
        response = rekcollect.list_faces()
        try:
            print("==================[ Faces ]==")
            for face in response['Faces']:
                print("--[ " + face['ExternalImageId'] + " ]--")
                print("ExternalImageId: " + face['ExternalImageId'])
                print("ImageId: " + face['ImageId'])
                print("FaceId: " + face['FaceId'])
                print("--------------------\n")
            print("=============================")
        except:
            print(response)

    if args.function == "face-delete":
        try:
            response = rekcollect.delete_faces()
            print(response)
        except Exception as e:
            rootLogger.error("Unable to delete FaceID %s %s", self.faceID, e) 

def _get_args():
    parser = ArgumentParser(description='AWS rekognition face indexing functions')
    
    parser.add_argument('-f', '--function', help="collection-create|delete|list, face-index|find|list|delete")
    parser.add_argument('-i', '--image', help="image path")
    parser.add_argument('-c', '--collection', default="piBell", help="collection name")
    parser.add_argument('--faceID', help="FaceID required to Delete ID. Run function face-list to view FaceID.")
    parser.add_argument('-l', '--label', help="image label. Typically person's name.")
    parser.add_argument('-s', '--secret', help='AWS secret access key')
    parser.add_argument('-a', '--access', help='AWS access key id')
    parser.add_argument('-r', '--region', default='us-west-2', help='AWS region. Default: us-west-2')
    parser.add_argument('-v', '--verbose', help='Output level. "INFO" is the default. Supports: "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"', default="INFO")
    return parser.parse_args()

class RekCollection(object):
    def __init__(self, awsAccessKey, awsSecAccessKey, awsRegion="us-west-2", debugMode="INFO"):
        self.filePath = "./image.jpg"
        self.collectionName = 'piBell'
        self.label = ''
        self.minAccuracy = 90
        self.awsAccessKey = awsAccessKey
        self.awsSecAccessKey = awsSecAccessKey
        self.awsRegion = awsRegion
        self.__rek = boto3.client('rekognition', region_name=awsRegion, aws_access_key_id=awsAccessKey, aws_secret_access_key=awsSecAccessKey)
        rootLogger.setLevel(debugMode)

    def _get_image(self):
        try:
            with open(self.filePath, 'rb') as imgfile:
                return imgfile.read()
        except Exception as error:
            rootLogger.error("Unable to open image: %s", error)
    
    def create_collection(self):
        try:
            rekresponse = self.__rek.create_collection(CollectionId=self.collectionName)
            return rekresponse
        except (BotoCoreError, ClientError) as error:
            rootLogger.error("Unable to create collection: %s", error)
    
    def delete_collection(self):
        try:
            rekresponse = self.__rek.delete_collection(CollectionId=self.collectionName)
            return rekresponse
        except (BotoCoreError, ClientError) as error:
            rootLogger.error("Unable to get delete collection: %s", error)
    
    def list_collection(self):
        try:
            rekresponse = self.__rek.list_collections()
            return rekresponse
        except (BotoCoreError, ClientError) as error:
            rootLogger.error("Unable to get list collection: %s", error)
    
    def index_face(self):
        try:
            rawImage = self._get_image()
            rekresponse = self.__rek.index_faces(CollectionId=self.collectionName, Image={'Bytes': rawImage}, ExternalImageId=self.label)
            return rekresponse
        except (BotoCoreError, ClientError) as error:
            rootLogger.error("Unable to get index faces: %s", error)
    
    def list_faces(self):
        try:
            rekresponse = self.__rek.list_faces(CollectionId=self.collectionName)
            return rekresponse
        except (BotoCoreError, ClientError) as error:
            rootLogger.error("Unable to get list faces: %s", error)
    
    def delete_faces(self):
        try:
            rekresponse = self.__rek.delete_faces(CollectionId=self.collectionName,FaceIds=[self.faceID])
            return rekresponse
        except (BotoCoreError, ClientError) as error:
            rootLogger.error("Unable to get list faces: %s", error)
    
    def find_face(self):
        try:
            rawImage = self._get_image()
            rekresponse = self.__rek.search_faces_by_image(CollectionId=self.collectionName, Image={'Bytes': rawImage}, FaceMatchThreshold=self.minAccuracy)
            return rekresponse
        except (BotoCoreError, ClientError) as error:
            rootLogger.error("Unable to get find face: %s", error)

if __name__ == "__main__":
    main()

