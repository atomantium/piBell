#!/usr/bin/env python3

import time
import Rekognition
import gvision
import gstorage
import faceDB
import TextToLCD
import PiPhoto
import Voice
import SNS
import motion
import json
import logging
import re
import signal, sys
from argparse import ArgumentParser

logFormatter = logging.Formatter("%(asctime)s [%(name)-8.8s]/[%(funcName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger(__name__)

fileHandler = logging.FileHandler("{0}/{1}.log".format("./", "piBell"), 'a')
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)


def main():
    TextToLCD.Clear()
    voice = Voice.Synthesizer(awsID, awsKey, awsRegion, voiceId, debugMode)

    rootLogger.info("Starting motion detection...")
    TextToLCD.ProcessFrameBuffer(["Scanning.......", "     For Motion"], debugMode)
   
    while not DetectMotion(): 
       time.sleep(0.1)

    counter = 0 
    while counter < 3:
        TextToLCD.ProcessFrameBuffer(['Taking a picture', '  Say Cheese!'])
        PiPhoto.GetPhoto(imgPath, imgRotation, debugMode, showImage)
       
        if counter == 2: 
            TextToLCD.ProcessFrameBuffer(['Last attempt to','Find a Face.'])
        else:
            TextToLCD.ProcessFrameBuffer(['Scanning.......', '    For a Face'])
        
        if gcpTestFace():
            TextToLCD.ProcessFrameBuffer(['Checking your ','Authorization.'], debugMode)
        
            gcpFileName = gUploadImg(imgPath)
            authUser = IsAuth()
            if authUser:
                userName = authUser.replace("_", ' ')
                TextToLCD.ProcessFrameBuffer(["Welcome", userName], debugMode)
                voice.Say("Welcome " + userName + ". Your authorization has been approved.")
                SendSms(userName + " has been Authorized. https://storage.cloud.google.com/" + gbucket + "/" + gcpFileName)
                rootLogger.debug("Attempting to Index new image as " + authUser)
                # Future: Need to count how many images have been indexs for authUser and if greater then X dont add any more indexes (AWS costs $.01 per index, no need to over index)
                IndexFace(authUser)
            else: 
                TextToLCD.ProcessFrameBuffer(["You shall", "  not pass!"], debugMode)
                voice.Say("Your authorization has been denied! You shall not pass!")
                SendSms("Unknown user has attempted unauthorized access! https://storage.cloud.google.com/" + gbucket + "/" + gcpFileName)
    
            celeb = IsCelebrity()
            if celeb:
                TextToLCD.ProcessFrameBuffer(["You look like:", celeb], debugMode)
                voice.Say("By the way, you remind me of " + celeb + "." )
            else:
                rootLogger.debug("Not able to detect a Celebrity for this face.")
            
            # Break out of While Counter loop as we have found a face
            break
        else:
            rootLogger.info("No face detected.")
            TextToLCD.ProcessFrameBuffer(['No face detected', '       :('], debugMode)
            time.sleep(0.5) 

        if counter >= 2:
            TextToLCD.ProcessFrameBuffer(['I give up,','I cant see a face clearly enough.'])
        counter += 1

def GetArgs():
    parser = ArgumentParser(description='piBell overrides')
    parser.add_argument('--show-image', help='Debugging with Captured image displayed in console output realtime. Supports: "True", "False"', default="False")
    parser.add_argument('-v', '--verbose', help='Output level. "INFO" is the default. Supports: "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"', default="INFO")
    parser.add_argument('-s', '--secret', help='AWS secret access key')
    parser.add_argument('-a', '--access', help='AWS access key id')
    parser.add_argument('-r', '--region', default='us-west-2', help='AWS region. Default: us-west-2')
    parser.add_argument('-b', '--bucket', default="pibell-bucket", help="GCP Bucket Name. Default: pibell-bucket")
    parser.add_argument('-p', '--project', default="pibell", help="GCP Project Name. Default: pibell")
    parser.add_argument('-i', '--image', default='./image.jpg', help='Image path. Default: ./image.jpg')
    parser.add_argument('-c', '--voice', default="Brian", help='Voice character for polly. Default: Brian')
    parser.add_argument('--rotation', default=270, help='Image rotation. Default: 270')
    parser.add_argument('--minconf', default=75, help='Level of confidence that a person is in the photo. Default: 75')
    parser.add_argument('--mincelebconf', default=1, help='Level of similarity to a celebirity to obtain a match. Default: 1')
    parser.add_argument('--minsimilarity', default=90, help='Minimum level of similarity for facial comparison. Default: 90')
    parser.add_argument('-n', '--number', help='Optional Phone Number for SMS. SMS will only be sent if number is provided. Format: +12223334444', default="none")
    return parser.parse_args()

def IsFace():
    rek = Rekognition.Rekognition(awsID, awsKey, awsRegion, debugMode)
    rek.filePath = imgPath
    try:
        jsonLabels = rek.get_labels()
        rootLogger.info("Obtained JSON payload from rekognition.")
        rootLogger.debug(json.dumps(jsonLabels))
        for value in jsonLabels:
            rootLogger.info("Label: " + value['Name'] + ", Confidence: " +  str(round(value['Confidence'])))
            rootLogger.debug(json.dumps(jsonLabels))
            if reFace.match(value['Name']) and round(value['Confidence']) > minConfidence:
                rootLogger.info("Possible face match.")
                return True    
    except Exception as e:
        rootLogger.info("Unable to obtain JSON response from rekognition: %s", e)
    rootLogger.info("No face match found.")
    return False

def gcpIsHand():
    gv = gvision.Vision(imgPath, debugMode)
    gv.filePath = imgPath
    try:
        gresponse = gv.google_detect_labels()
        rootLogger.debug(gresponse)
        for item in gresponse:
            if reHand.match(item.description):
                rootLogger.info("Found hand in image.")
                return True
    except Exception as e:
        rootLogger.error("Unable to obtain google vision response: %s", e)
    return False

def awsIsHand():
    rek = Rekognition.Rekognition(awsID, awsKey, awsRegion, debugMode)
    rek.filePath = imgPath
    try: 
        jsonLabels = rek.get_labels()
        rootLogger.debug(json.dumps(jsonLabels))
        for item in jsonLabels:
            if reHand.match(item['Name']):
                    rootLogger.info("Found hand in image.")
                    return True
    except Exception as e:
        rootLogger.error("Unable to obtain JSON response from rekognition: ", e)
    rootLogger.info("No hand found.")
    return False

def awsTestFace():
    rek = Rekognition.Rekognition(awsID, awsKey, awsRegion, debugMode)
    rek.filePath = imgPath
    try:
        jsonFaces = rek.get_faces()
        rootLogger.debug(json.dumps(jsonFaces))
        for item in jsonFaces:
            if item['Confidence'] > minConfidence:
                rootLogger.info("Face detected. Confidence: " + str(round(item['Confidence'])))
                rootLogger.debug(json.dumps(jsonFaces))
                return True
    except Exception as e:
        rootLogger.error("Unable to obtain JSON response from rekognition: ", e)
    rootLogger.info("No facial data obtained.")
    return False

def gcpTestFace():
    gv = gvision.Vision(imgPath, debugMode)
    gv.filePath = imgPath
    try:
        gresponse = gv.google_detect_face()
        rootLogger.debug(gresponse)
        if len(gresponse.face_annotations) > 0:
            rootLogger.info("Google vision found a face.")
            return True
    except Exception as e:
        rootLogger.error("Unable to obtain google vision response: %s", e)
    return False

def IsCelebrity():
    rek = Rekognition.Rekognition(awsID, awsKey, awsRegion, debugMode)
    rek.filePath = imgPath
    try:
        jsonCelbFaces = rek.get_celebrities()
        rootLogger.debug(json.dumps(jsonCelbFaces))
        for item in jsonCelbFaces:
            if item['MatchConfidence'] > minCelebConf and item['Name']:
                rootLogger.info("Celebirity match detected: " + item['Name'] + ", Confidence: " + str(round(item['MatchConfidence'])))
                rootLogger.debug(json.dumps(jsonCelbFaces))
                return item['Name']
    except Exception as e:
        rootLogger.info("Unable to obtain JSON response from rekognition: %s", e)
    rootLogger.info("No celebirity match found.")
    return False

def IsAuth():
    rekcollect = faceDB.RekCollection(awsID, awsKey, awsRegion, debugMode)
    rekcollect.collectionName = 'piBell'
    rekcollect.filePath = imgPath
    rekcollect.minAccuracy = minSimilarity
    try:
        jsonFaces = rekcollect.find_face()
        for faceMatch in jsonFaces['FaceMatches']:
            rootLogger.info("Face comparison returned match: " + faceMatch['Face']['ExternalImageId'])
            return faceMatch['Face']['ExternalImageId']
    except Exception as e:
        rootLogger.error("Unable to obtain response from rekognition: %s", e)
    return False

def IndexFace(userName):
    rekcollect = faceDB.RekCollection(awsID, awsKey, awsRegion, debugMode)
    rekcollect.collectionName = 'piBell'
    rekcollect.filePath = imgPath
    rekcollect.label = userName
    try:
        rekresponse = rekcollect.index_face()
        rootLogger.debug(rekresponse)
        rootLogger.info("Successfully added %s to %s's Rekognition Index.", rekcollect.filePath, rekcollect.label)
        return True 

    except Exception as e:
        rootLogger.error("Unable to index additional image for %s with rekognition: %s", userName, e)
    return False

def SendSms(messageBody):
    sms = SNS.SMS(awsID, awsKey, awsRegion, messageBody, number, debugMode)
    try: 
        sms.send_sms()
    except Exception as e:
        rootLogger.error("Unable to send SMS: %s", e)
    return False

def DetectMotion():
    mo = motion.Motion(debugMode)
    try: 
        response = mo.detectMotion()
        return response
    except Exception as e:
        rootLogger.error("Unable to detect Motion: %s", e)
    return False

def gUploadImg(filePath):
    date = time.strftime("%Y%m%d-%H.%M.%S", time.localtime())
    fileName = (date + ".jpg")
    fileType = "image/jpeg"
    gs = gstorage.Storage(fileName, filePath, fileType, gbucket, gproject, debugMode)
    try: 
        gs.upload_from_filename()
        return fileName
    except Exception as e:
        rootLogger.error("Unable to upload " + filePath + " to " + gproject + "/" + gbucket + ": %s", e)
    return False

def SignalHandler(signal, frame):
    print("Exiting application.")
    TextToLCD.Clear()
    sys.exit(0)

if __name__ == "__main__":
    args = GetArgs()
    debugMode = args.verbose
    showImage = args.show_image
    awsID = args.access
    awsKey = args.secret
    awsRegion = args.region
    gbucket = args.bucket
    gproject = args.project
    imgRotation = args.rotation
    imgPath = args.image
    minConfidence = args.minconf
    minSimilarity = args.minsimilarity
    minCelebConf = args.mincelebconf
    voiceId = args.voice
    number = args.number
    reFace = re.compile('face|head|selfie|portrait|person', re.IGNORECASE)
    reHand = re.compile('hand|finger|arm', re.IGNORECASE)
    rootLogger.setLevel(debugMode)
    SendSms("\nPiBell has been Initalized\n")

    signal.signal(signal.SIGINT, SignalHandler)

    while True: 
        main()
